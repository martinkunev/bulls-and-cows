CC=gcc
CFLAGS=-std=c99 -pedantic -pthread -O0 -g -fomit-frame-pointer -D_DEFAULT_SOURCE
#CFLAGS=-std=c99 -pedantic -pthread -O2 -fomit-frame-pointer -DNDEBUG -D_DEFAULT_SOURCE
LDFLAGS= -pthread -lm -lcurses

all: bulls_cows

bulls_cows: engine.o bulls_cows.o
	$(CC) $(CFLAGS) $^ $(LDFLAGS) -o $@

test: engine.o test.o

clean:
	rm -f *.o
	rm -f bulls_cows test
