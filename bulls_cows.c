/*
 * Bulls and Cows
 * Copyright (C) 2021  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Bulls and Cows.
 *
 * Bulls and Cows is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Bulls and Cows is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Bulls and Cows.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <curses.h>
#include <pthread.h>

#include "engine.h"

#define DIGIT_INVALID 127

struct printable
{
	char printable[DIGITS_LIMIT + 4 + 1]; // e.g. "1234 0 2\0"
};

#define ARRAY_TYPE struct printable
#include "array.g"

// TODO support scrolling
// TODO support resize?
// 	case KEY_RESIZE:
//  both of the above need refactoring of input handling

struct bytes
{
	size_t size;
	unsigned char data[];
} __attribute__((aligned(1)));

#define bytes(init) &((union {struct {size_t size; unsigned char data[sizeof(init)];} __attribute__((aligned(1))) _; struct bytes bytes;}){sizeof(init) - 1, init}).bytes

struct input
{
	char buffer[6];
	unsigned position;
};

static const unsigned char digit_value[] =
{
	['0'] = 0, ['1'] = 1, ['2'] = 2, ['3'] = 3, ['4'] = 4,
	['5'] = 5, ['6'] = 6, ['7'] = 7, ['8'] = 8, ['9'] = 9,
	['a'] = 10, ['A'] = 10, ['b'] = 11, ['B'] = 11, ['c'] = 12, ['C'] = 12,
	['d'] = 13, ['D'] = 13, ['e'] = 14, ['E'] = 14, ['f'] = 15, ['F'] = 15,
	[' '] = DIGIT_INVALID,
};

static int input_guess(unsigned digits, const struct input *restrict info, struct number *restrict result)
{
	size_t i;

	memset(result->position, -1, sizeof(result->position));

	for(i = 0; i < digits; i += 1)
	{
		char digit = digit_value[info->buffer[i]];
		if ((digit == DIGIT_INVALID) || (result->position[digit] >= 0))
			return 0;
		result->position[digit] = i;
		result->digit[i] = digit;
	}

	return 1;
}

static struct number input_read(WINDOW *restrict textbox, WINDOW *restrict message, const struct game *restrict game)
{
	struct input input = {0};

	assert(sizeof(input.buffer) >= game->digits + 1);

	memset(input.buffer, ' ', game->digits);
	mvwprintw(textbox, 1, 1, input.buffer);

	while (1)
	{
		int c;

		wmove(textbox, 1, 1 + input.position);

		switch (c = wgetch(textbox))
		{
		case KEY_LEFT:
			if (!input.position)
				continue;
			input.position -= 1;
			break;

		case 127:
			if (!input.position)
				continue;
			input.position -= 1;
			input.buffer[input.position] = ' ';
			break;

		case KEY_RIGHT:
			if (input.position == game->digits)
				continue;
			input.position += 1;
			break;

		case '\n':
			{
				struct number guess;
				if (!input_guess(game->digits, &input, &guess))
				{
					mvwprintw(message, 0, 0, "Invalid number");
					wrefresh(message);
					continue;
				}
				return guess;
			}
			break;

		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 'a':
		case 'A':
		case 'b':
		case 'B':
		case 'c':
		case 'C':
		case 'd':
		case 'D':
		case 'e':
		case 'E':
		case 'f':
		case 'F':
			{
				char lower = tolower(c);
				unsigned char value = digit_value[lower];

				if (input.position == game->digits)
					continue; // no place left for more digits
				if (value >= game->base)
					continue; // invalid digit

				input.buffer[input.position++] = lower;
			}
			break;

		case 'q':
			exit(0);
		}

		werase(message);
		wrefresh(message);
		mvwprintw(textbox, 1, 1, input.buffer);
	}
}

static inline void remwin(WINDOW *window)
{
	werase(window);
	wrefresh(window);
	delwin(window);
}

static enum difficulty input_difficulty(void)
{
	enum difficulty difficulty = MEDIUM;

	WINDOW *window;

	window = newwin(5, 8, 9, 1);
	keypad(window, TRUE);
	box(window, 0, 0);

	mvwprintw(window, 1, 1, "Easy");
	mvwprintw(window, 2, 1, "Normal");
	mvwprintw(window, 3, 1, "Hard");

	while (1)
	{
		wmove(window, 1 + difficulty, 1);
		wrefresh(window);
		switch (wgetch(window))
		{
		case KEY_UP:
			if (difficulty != EASY)
				difficulty -= 1;
			break;

		case KEY_DOWN:
			if (difficulty != HARD)
				difficulty += 1;
			break;

		case '\n':
			remwin(window);
			return difficulty;		

		case 'q':
			exit(0);
		}
	}
}

static const char *printable(const struct game *restrict game, const struct number *restrict number)
{
	static char buffer[DIGITS_LIMIT + 1];
	size_t i;
	for(i = 0; i < game->digits; i += 1)
		buffer[i] = "0123456789abcdef"[number->digit[i]];
	buffer[game->digits] = 0;
	return buffer;
}

static void guess_append(const struct game *restrict game, struct array *restrict guesses, const struct number *restrict guess, struct score score)
{
	char *start;

	if (array_expand(guesses, guesses->count + 1) < 0)
		abort();

	start = guesses->data[guesses->count].printable;
	memcpy(start, printable(game, guess), game->digits);
	start[game->digits] = ' ';
	start[game->digits + 1] = score.match[BULLS] + '0';
	start[game->digits + 2] = ' ';
	start[game->digits + 3] = score.match[COWS] + '0';
	start[game->digits + 4] = 0;

	guesses->count += 1;
}

static void display_guesses(WINDOW *window, const struct array *restrict guesses)
{
	size_t i;
	const unsigned lines = LINES - 9 - 3;
	size_t offset = (guesses->count > lines) ? (guesses->count - lines) : 0;

	for(i = offset; i < guesses->count; i += 1)
		mvwprintw(window, 2 + (i - offset), 1, guesses->data[i].printable);

	wrefresh(window);
}

static void menu(struct game *restrict game)
{
	WINDOW *label, *textbox, *message;

	textbox = newwin(3, game->digits + 3, 4, 1);
	keypad(textbox, TRUE);
	message = newwin(1, COLS - 2, LINES - 2, 1);

	// secret dialog
	{
		label = newwin(1, COLS - 2, 3, 2);
		mvwprintw(label, 0, 0, "secret:");
		wrefresh(label);

		box(textbox, 0, 0);
		game->secret_local = input_read(textbox, message, game);

		remwin(label);
	}

	// difficulty dialog
	{
		label = newwin(1, COLS - 2, 8, 2);
		mvwprintw(label, 0, 0, "difficulty:");
		wrefresh(label);

		game->difficulty = input_difficulty();

		remwin(label);
	}

	remwin(message);
	remwin(textbox);
}

static void play(const struct game *restrict game)
{
	struct array guesses_local = {0}, guesses_computer = {0};
	unsigned turns_local = 0, turns_computer = 0; // number of turns to guess the secret of the opponent
	unsigned turn = 1;
	size_t i;

	struct info info = {.game = game};

	WINDOW *textbox, *message, *guesses, *enemy;

	textbox = newwin(3, game->digits + 3, LINES - 5, 1);
	keypad(textbox, TRUE);
	message = newwin(1, COLS - 2, LINES - 2, 1);
	guesses = newwin(LINES - 9, game->digits + 6, 3, 1);
	enemy = newwin(LINES - 9, game->digits + 6, 3, 17);

	box(guesses, 0, 0);
	mvwprintw(guesses, 1, 1 + game->digits + 1, "B C");

	box(enemy, 0, 0);
	mvwprintw(enemy, 1, 1 + game->digits + 1, "B C");

	box(textbox, 0, 0);

	info.count_max = 1;
	for(i = 0; i < game->digits; i += 1)
		info.count_max *= game->base - i;
	info.possible = malloc(info.count_max * sizeof(*info.possible));
	if (!info.possible)
		abort();
	info.count = 0;

	do
	{
		struct number guess;
		struct score score;

		if (!turns_computer)
		{
			guess = computer_guess(&info, game->base, game->digits);
			score = match(game, guess.position, game->secret_local);
			if (score.match[BULLS] != game->digits)
				computer_update(&info, guess.position, score);
			guess_append(game, &guesses_computer, &guess, score);
			if (score.match[BULLS] == game->digits)
				turns_computer = turn; // computer guessed successfully
		}

		if (!turns_local)
		{
			// Display previous guesses.
			display_guesses(guesses, &guesses_local);
			display_guesses(enemy, &guesses_computer);

			guess = input_read(textbox, message, game);
			score = match(game, guess.position, game->secret_computer);
			guess_append(game, &guesses_local, &guess, score);
			if (score.match[BULLS] == game->digits)
				turns_local = turn; // local player guessed successfully
		}

		turn += 1;
	} while (!turns_local || !turns_computer);

	display_guesses(guesses, &guesses_local);
	display_guesses(enemy, &guesses_computer);
	wgetch(message);

	remwin(enemy);
	remwin(guesses);
	remwin(message);
	remwin(textbox);
}

static void terminate(void)
{
	endwin();
}

int main(int argc, char *argv[])
{
	struct game game = {.digits = 4, .base = 10};

	struct bytes *title = bytes("Bulls and Cows");

	WINDOW *secret;

	if (argc > 1)
	{
		char *end = 0;

		if (argc > 2)
		{
			if (argc > 3)
			{
				fprintf(stderr, "usage: %s [digits] [base]\n", argv[0]);
				return 1;
			}

			game.base = strtol(argv[2], &end, 10);
			if ((end == argv[2]) || (game.base < 2) || (game.base > BASE_LIMIT))
			{
				fprintf(stderr, "Invalid base. Try 8 or 10 or 16.\n");
				return 3;
			}
		}

		game.digits = strtol(argv[1], &end, 10);
		if ((end == argv[1]) || (game.base < 2) || (game.base > DIGITS_LIMIT))
		{
			fprintf(stderr, "Invalid digits. Try 3 or 4 or 5.\n");
			return 3;
		}
	}

	game.secret_computer = guess_random(&game);

	// getmaxyx(..., y, x); // WARNING: macro

	initscr();
	raw();
	noecho();
	keypad(stdscr, TRUE);

	if (atexit(&terminate))
		abort();

	mvprintw(1, (COLS - title->size) / 2, (const char *)title->data);
	refresh();
	menu(&game);

	secret = newwin(1, game.digits + 8, LINES - 4, 17);
	mvwprintw(secret, 0, 0, "secret: %s", printable(&game, &game.secret_local));
	wrefresh(secret);
	play(&game);
	delwin(secret);

	terminate();

	return 0;
}
