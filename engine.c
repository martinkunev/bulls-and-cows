/*
 * Bulls and Cows
 * Copyright (C) 2021  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Bulls and Cows.
 *
 * Bulls and Cows is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Bulls and Cows is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Bulls and Cows.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <math.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#include "engine.h"

void number_positions(const struct game *restrict game, struct number *restrict number)
{
	size_t i;
	memset(number->position, -1, game->base);
	for(i = 0; i < game->digits; i += 1)
		number->position[number->digit[i]] = i;
}

struct number guess_random(const struct game *game)
{
	unsigned char list[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
	size_t i;

	struct number guess;

	assert(sizeof(list) == BASE_LIMIT);

	for(i = 0; i < game->digits; i += 1)
	{
		size_t chance = i + random() % (game->base - i);
		guess.digit[i] = list[chance];
		list[chance] = list[i];
	}

	number_positions(game, &guess);
	return guess;
}

struct score match(const struct game *restrict game, const signed char *restrict guess_positions, struct number secret)
{
	struct score score = {0};
	size_t i;
	for(i = 0; i < game->digits; i += 1)
	{
		signed char position = guess_positions[secret.digit[i]];
		score.match[position == i] += (position >= 0);
	}
	return score;
}

static struct positions number2positions(const struct game *restrict game, struct number number)
{
	size_t i;
	struct positions positions;
	memset(positions.digits, -1, game->base);
	for(i = 0; i < game->digits; i += 1)
		positions.digits[number.digit[i]] = i;
	return positions;
}

static void secrets_populate(struct info *restrict info, unsigned char *restrict digits, size_t left)
{
	size_t i;

	if (!left)
	{
		memcpy(info->possible[info->count++].digit, digits, info->game->digits);
		assert(info->count <= info->count_max);
		return;
	}

	secrets_populate(info, digits, left - 1);

	for(i = info->game->digits - left + 1; i < info->game->base; i += 1)
	{
		unsigned char digit = digits[info->game->digits - left];
		digits[info->game->digits - left] = digits[i];
		digits[i] = digit;

		secrets_populate(info, digits, left - 1);

		digits[i] = digits[info->game->digits - left];
		digits[info->game->digits - left] = digit;
	}
}

static void secrets_filter(struct info *info)
{
	size_t i, end;

	// Filter out impossible secrets.
	end = 0;
	for(i = 0; i < info->count; i += 1)
	{
		struct score score = match(info->game, info->positions, info->possible[i]);
		if ((score.match[BULLS] == info->score.match[BULLS]) && (score.match[COWS] == info->score.match[COWS]))
		{
			if (end < i)
				info->possible[end] = info->possible[i];
			end += 1;
		}
	}
	info->count = end;
}

static void *computer_next_easy(void *argument)
{
	struct info *info = argument;
	size_t chance;
	struct number guess;

	secrets_filter(info);
	if (info->count == 1)
	{
		memcpy(&info->guess, info->possible, sizeof(*info->possible));
		return 0;
	}

	// Make a random guess.
	guess = guess_random(info->game);
	memcpy(&info->guess, &guess, sizeof(guess));

	return 0;
}

static void *computer_next_medium(void *argument)
{
	struct info *info = argument;
	size_t chance;

	secrets_filter(info);
	if (info->count == 1)
	{
		memcpy(&info->guess, info->possible, sizeof(*info->possible));
		return 0;
	}

	// Make a random guess among the possible numbers.
	chance = random() % info->count;
	memcpy(&info->guess, info->possible + chance, sizeof(*info->possible));

	return 0;
}

static inline size_t match_index(struct score score)
{
	return score.match[BULLS] * (DIGITS_LIMIT + 1) + score.match[COWS];
}

// TODO optimization needed: for each of the 5400 numbers, I'm cycling each of the possible correct numbers and performing operations for each digit. this can come to about 20 million iterations

static void guess_foreach(struct info *info, unsigned char *restrict guess, size_t left)
{
	size_t i;

	if (!left)
	{
		struct positions positions;
		unsigned counts[DIGITS_LIMIT * (DIGITS_LIMIT + 1)] = {0};
		const size_t index_correct = match_index((struct score){.match[BULLS] = info->game->digits});
		double entropy = 0;

		struct number number;
		memcpy(number.digit, guess, info->game->digits);

		positions = number2positions(info->game, number);

		// Calculate the entropy of guess info->possible[i].
		for(i = 0; i < info->count; i += 1)
		{
			struct score score = match(info->game, positions.digits, info->possible[i]);
			size_t index = match_index(score);
			counts[index] += 1;
		}
		for(i = 0; i < sizeof(counts) / sizeof(*counts); i += 1)
			if (counts[i] > 1)
				entropy -= counts[i] * log2(counts[i]);
		entropy = log2(info->count) + entropy / info->count;

		if (entropy > info->entropy_best)
		{
			// Store whether the current guess could be correct.
			info->entropy_best = entropy;
			info->guess = number;
			info->sequence = 1;
			info->correct_match = (counts[index_correct] != 0);
		}
		else if (entropy && (entropy == info->entropy_best))
		{
			if (info->correct_match == (counts[index_correct] != 0))
			{
				// Multiple highest entropy guesses are possible secrets, choose one of them randomly.
				info->sequence += 1;
				if (!(random() % info->sequence))
					info->guess = number;
			}
			else if (counts[index_correct])
			{
				// Old guess cannot be correct. Replace it with the current one as it could be correct.
				info->entropy_best = entropy;
				info->guess = number;
				info->sequence = 1;
				info->correct_match = 1;
			}
		}

		return;
	}

	guess_foreach(info, guess, left - 1);

	for(i = info->game->digits - left + 1; i < info->game->base; i += 1)
	{
		unsigned char digit = guess[info->game->digits - left];
		guess[info->game->digits - left] = guess[i];
		guess[i] = digit;

		guess_foreach(info, guess, left - 1);

		guess[i] = guess[info->game->digits - left];
		guess[info->game->digits - left] = digit;
	}
}

static void *computer_next_hard(void *argument)
{
	struct info *info = argument;
	unsigned char list[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

	secrets_filter(info);
	if (info->count == 1)
	{
		memcpy(&info->guess, info->possible, sizeof(*info->possible));
		return 0;
	}

	// Choose a guess with maximum shannon entropy.
	info->entropy_best = 0;
	guess_foreach(info, list, info->game->digits);

	return 0;
}

/*#include <stdio.h>
static void print(struct number number, unsigned length)
{
	size_t i;
	for(i = 0; i < length; i += 1)
	{
		static const unsigned char digits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
		putchar(digits[number.digit[i]]);
	}
	putchar('\n');
}*/

struct number computer_guess(struct info *info, unsigned base, unsigned digits)
{
	struct number guess;

	if (info->count) // secrets already populated
	{
		void *result;

		pthread_join(info->id, &result);
		memcpy(&guess, &info->guess, digits);
		number_positions(info->game, &guess);
		//print(guess, digits);
		return guess;
	}
	else
	{
		// Choose first guess randomly to confuse adversaries.
		/*struct number n = guess_random(info->game);
		print(n, digits);
		return number2positions(info->game, n);*/
		return guess_random(info->game);
	}
}

void computer_update(struct info *restrict info, signed char *restrict positions, struct score score)
{
	memcpy(info->positions, positions, BASE_LIMIT * sizeof(*positions));
	info->score = score;

	if (!info->count)
	{
		unsigned char list[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
		secrets_populate(info, list, info->game->digits);
	}

	switch (info->game->difficulty)
	{
	case EASY:
		pthread_create(&info->id, 0, &computer_next_easy, info);
		break;

	case MEDIUM:
		pthread_create(&info->id, 0, &computer_next_medium, info);
		break;

	case HARD:
		pthread_create(&info->id, 0, &computer_next_hard, info);
		break;
	}
}
