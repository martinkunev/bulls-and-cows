/*
 * Bulls and Cows
 * Copyright (C) 2021  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Bulls and Cows.
 *
 * Bulls and Cows is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Bulls and Cows is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Bulls and Cows.  If not, see <http://www.gnu.org/licenses/>.
 */

#define DIGITS_LIMIT 5
#define BASE_LIMIT 16

enum {COWS = 0, BULLS = 1};

struct score
{
	unsigned char match[2];
};

struct number
{
	unsigned char digit[DIGITS_LIMIT];
	signed char position[BASE_LIMIT]; // -1 indicates digit not present
};

struct positions
{
	// -1 indicates digit not present
	signed char digits[BASE_LIMIT];
};

struct game
{
	unsigned base;
	unsigned digits;
	struct number secret_local, secret_computer;
	enum difficulty {EASY, MEDIUM, HARD} difficulty;
};

struct info
{
	const struct game *game;

	struct number *possible;
	size_t count, count_max;

	struct score score;
	signed char positions[BASE_LIMIT];
	struct number guess;

	double entropy_best;
	unsigned sequence;
	unsigned correct_match;

	pthread_t id;
};

struct score match(const struct game *restrict game, const signed char *restrict number, struct number secret);

struct number computer_guess(struct info *info, unsigned base, unsigned digits);
void computer_update(struct info *restrict info, signed char *restrict positions, struct score score);

void number_positions(const struct game *restrict game, struct number *restrict number);
struct number guess_random(const struct game *game);
