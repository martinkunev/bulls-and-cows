/*
 * Bulls and Cows
 * Copyright (C) 2021  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Bulls and Cows.
 *
 * Bulls and Cows is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Bulls and Cows is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Bulls and Cows.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "engine.h"

#define N 16

// TODO abstraction problems: struct info

static void print(struct number number, unsigned length)
{
	size_t i;
	for(i = 0; i < length; i += 1)
	{
		static const unsigned char digits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
		putchar(digits[number.digit[i]]);
	}
}

static unsigned play(const struct game *restrict game, struct info *restrict info)
{
	unsigned turn = 1;

	while (1)
	{
		struct number guess;
		struct score score;

		guess = computer_guess(info, game->base, game->digits);
		score = match(game, guess.position, game->secret_local);
		if (score.match[BULLS] != game->digits)
			computer_update(info, guess.position, score);

		if (score.match[BULLS] == game->digits)
			return turn;

		turn += 1;
	}
}

int main()
{
	struct game game;
	unsigned turn;

	struct info info = {0};

	size_t i;

	srandom(time(0));

	game.base = 10;
	game.digits = 4;

	game.secret_local = (struct number){9, 6, 3, 7};
	game.secret_computer = (struct number){9, 8, 7, 6};

	info.count_max = 1;
	for(i = 0; i < game.digits; i += 1)
		info.count_max *= game.base - i;
	info.possible = malloc(info.count_max * sizeof(*info.possible));
	if (!info.possible)
	{
		fprintf(stderr, "memory allocation error\n");
		abort();
	}
	info.game = &game;

///////////////////////////////

	unsigned good = 0, neutral = 0, bad = 0;
	unsigned long tt[3] = {0};

	print(game.secret_local, game.digits);
	putchar('\n');

	for(i = 0; i < N; i += 1)
	{
		unsigned t;

		info.count = 0;
		game.difficulty = EASY;
		turn = play(&game, &info);
		tt[0] += turn;
		//	printf("TURNS: %u\n\n", turn);

		info.count = 0;
		game.difficulty = MEDIUM;
		turn = play(&game, &info);
		tt[1] += turn;
		//	printf("TURNS: %u\n\n", turn);
		t = turn;

		info.count = 0;
		game.difficulty = HARD;
		turn = play(&game, &info);
		tt[2] += turn;
		//printf("TURNS: %u\n\n", turn);

		if (t > turn)
			good += 1;
		else if (t < turn)
			bad += 1;
		else
			neutral += 1;
	}

	printf("good=%u neutral=%u bad=%u\n", good, neutral, bad);
	printf("easy=%f medium=%f hard=%f\n", tt[0] / (double)N, tt[1] / (double)N, tt[2] / (double)N);

	free(info.possible);

	return 0;
}
